var Ajedrez = (function() {

var _mostrar_tablero = function(){
  
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      
      var tablero = document.getElementById("tablero");
      
      var str = this.responseText.split('\r\n');
      
      var tabla   = document.createElement("table");
      var tBody = document.createElement("tbody");
      var letras = str[0].split('|');
      var pos = 8;
      for (var i = 1; i < str.length; i++) {
        var fila = document.createElement("tr");
        var f = str[i].split('|');
        
        for (var j = 0; j < f.length; j++) {
          var ficha = document.createElement('td');
          if(f[j] != '0')
            ficha.innerHTML = f[j];
          ficha.id = letras[j]+(pos);
          ficha.style.width = '50px';
          ficha.style.height = '50px';
          fila.appendChild(ficha);
        }
        tBody.appendChild(fila);
        pos--;
      }

      tabla.appendChild(tBody);
      tablero.appendChild(tabla);

      for (var i = 0; i < letras.length; i++) {
        for (var j = 0; j < 8; j++) {
          var tipo=(i%2)?"Impar":"Par";
          var tipo2 =(j%2)?"Impar":"Par";
          console.log(tipo);
          if(tipo == 'Impar'){
            if(tipo2 == 'Impar')
              document.getElementById(letras[i]+(j+1)).style.backgroundColor = "#d65b11";
            else
              document.getElementById(letras[i]+(j+1)).style.backgroundColor = "#e0cb8e";
          }else{
            if(tipo2 == 'Par')
              document.getElementById(letras[i]+(j+1)).style.backgroundColor = "#d65b11";
            else
              document.getElementById(letras[i]+(j+1)).style.backgroundColor = "#e0cb8e";
          }
          
        }
      }
    }
  };
  xhttp.open("GET", "tablero.csv", true);
  xhttp.send();
};

var _actualizar_tablero = function(){
document.getElementById("tablero").innerHTML = "";
  _mostrar_tablero();
};

var _mover_pieza = function(obj_parametros){

var pr = obj_parametros.de;
    var se = obj_parametros.a;
if(document.getElementById(se).textContent ==  ""){
    document.getElementById(se).textContent=document.getElementById(pr).textContent;
    document.getElementById(pr).textContent="";
}
else{
alert("Ya se realizo el movimiento");
}
};

return {
  "mostrarTablero": _mostrar_tablero,
  "actualizarTablero": _actualizar_tablero,
  "moverPieza": _mover_pieza
}

})();
